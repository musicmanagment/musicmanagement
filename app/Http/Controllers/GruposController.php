<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grupo;
use App\Genero;
use App\Usuario;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class GruposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$grupos = Grupo::all();
        $response = Response::json($grupos, 200);
        return $response;*/


        $grupo = Grupo::all();
        $response = Response::json($grupo, 200);
        return $response;
        //$grupo->genero()->attach(1);
        //dd($grupo->genero);
        //dd($grupo);

        /*$idGrupo = 1;
        $grupos = Grupo::find($idGrupo);
        $grupo_genero = $grupos->generos->idGenero->toArray();
        $response = Response::json($grupo_genero, 200);
        return $response;*/

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$params = new Grupo;
        //var_dump($request);*/
        $json = $request->input('json', null);
        $params = json_decode($json);
/*
        DB::table('grupos')->insert(
            ['nombreGrupo' => $params->nombreGrupo, 
            'descripcion' => $params->descripcion,
            'imagenGrupoPerf' => $params->imagenGrupoPerf
            ]
        );
        $params->genero()->attach($params->idGenero);*/
        
        /*var_dump($params->nombreGrupo);
        die;*/
        /*var_dump($params->idUsuario);
            die;*/
        $grupo = Grupo::create([
            'nombreGrupo' => $params->nombreGrupo,
            'descripcion' => $params->descripcion,
            'imagenGrupoPerf' => $params->imagenGrupoPerf
        ]);
            
        $grupo->usuarios()->attach($params->usuario_id);
        $grupo->genero()->attach($params->idGenero);
        
        
        
    }







    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idGenero)
    {
        // print_r($idGenero);

        $result = DB::select('SELECT * FROM grupos, generos, genero_grupo where generos.idGenero = (:idGenero) and generos.idGenero = genero_grupo.genero_idGenero and grupos.idGrupo = genero_grupo.grupo_idGrupo ;', ['idGenero' => $idGenero]);
        // var_dump($result);

        // $grupos = Grupo::
        /*$grupos = Grupo::find($id);

        if (!$genero){
            return Response::json([
                'error' => ['message' => "No existe el genero"]
            ], 404);
        }*/
        //echo $result->nombreGrupo;
        return Response::json($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function perfil($nombreGrupo){
       // var_dump ($nombreGrupo);
       // $result = {'mensaje','hola Perfil'};


       $grupoSelect = DB::select('SELECT grupos.idGrupo, grupos.nombreGrupo, grupos.descripcion, grupos.imagenGrupoPerf FROM grupos WHERE grupos.nombreGrupo = (:nombreGrupo)', ['nombreGrupo' => $nombreGrupo]);
       $multimedia = DB::select('SELECT multimedia.idMultimedia, multimedia.nombreArchivo, multimedia.tipoArchivo, multimedia.urlMultimedia FROM grupos, multimedia, grupo_multimedia  where grupos.idGrupo = grupo_multimedia.grupo_idGrupo and multimedia.idMultimedia = grupo_multimedia.multimedia_idMultimedia and grupos.nombreGrupo = (:nombreGrupo);', ['nombreGrupo' => $nombreGrupo ]);
       //$result = DB::select('SELECT * FROM grupos, multimedia, grupo_multimedia  where grupos.idGrupo = grupo_multimedia.grupo_idGrupo and multimedia.idMultimedia = grupo_multimedia.multimedia_idMultimedia and grupos.nombreGrupo = (:nombreGrupo);', ['nombreGrupo' => $nombreGrupo ]);
       
       $gruMul[] = $grupoSelect;
       $gruMul[] = $multimedia;

        //$grupoMultimedia = json_encode($test);

       /* var_dump($gruMul);
       die;*/
        return Response::json($gruMul, 200);
    }



    public function crearGrupo(Request $request){
        var_dump($request);
        echo 'hola crear grupos';
    }


}
