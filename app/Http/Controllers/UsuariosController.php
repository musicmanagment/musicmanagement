<?php

namespace App\Http\Controllers;
use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Usuario;


class UsuariosController extends Controller
{

    public function index(){
       var_dump('hola');
    }

    public function show(Request $request){
        var_dump($request);
    }

    public function register(Request $request){
        var_dump($request);
        $json = $request->input('json', null);
        $params = json_decode($json);

        $nombreUsuario = (!is_null($json) && isset($params-> nombreUsuario)) ? $params->nombreUsuario : null;
        $apellidos = (!is_null($json) && isset($params-> apellidos)) ? $params->apellidos : null;
        $email = (!is_null($json) && isset($params-> email)) ? $params->email : null;
        $direccion = (!is_null($json) && isset($params-> direccion)) ? $params->direccion : null;
        $tipusUsuari = (!is_null($json) && isset($params-> tipusUsuari)) ? $params->tipusUsuari : null;
        $adminGrupo = (!is_null($json) && isset($params-> adminGrupo)) ? $params->adminGrupo : null;
        $password = (!is_null($json) && isset($params-> password)) ? $params->password : null;


        if(!is_null($nombreUsuario) && !is_null($email) && !is_null($tipusUsuari) && !is_null($adminGrupo) && !is_null($password)){
            // crear el usuario
            $usuario = new Usuario();
            $usuario->nombreUsuario = $nombreUsuario;
            $usuario->apellidos = $apellidos;
            $usuario->email = $email;
            $usuario->direccion = $direccion;
            $usuario->tipusUsuari = $tipusUsuari;
            $usuario->adminGrupo = $adminGrupo;

            $pwd = hash('sha256', $password);
            $usuario->password = $pwd;

            //Comprobar usuario duplicado

            $isset_user = Usuario::where('email', '=', $email)->first();

            if(count($isset_user)==0){
                $usuario->save();

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Usuario registrado correctamente.'
                );

                /*$response = Response::json([
                    'status' => 'success',
                    'message' => 'Usuario registrado correctamente.',
                ], 200);*/



            }else{
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Ya existe un usuario registrado con este email.'
                );

                /*$response = Response::json([
                    'status' => 'error',
                    'message' => 'Ya existe un usuario registrado con este email.'
                ], 400);*/

            }

        }else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no creado'
            );
              /*  $response = Response::json([
                    'status' => 'error',
                    'message' => 'Usuario no creado'
                ], 400);*/


        }

        return response()->json($data, 200);
        // return $response;

    }

    public function login (Request $request){
        // var_dump('hola'); die();
        $jwtAuth = new JwtAuth();



        //Recibir Post
        $json = $request->input('json', null);
        $params = json_decode($json);



        $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
        $getToken = (!is_null($json) && isset($params->gettoken)) ? $params->gettoken : null;


        //Cifrar Password

        $pwd = hash('sha256', $password);

        if(!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false')){
            $signup = $jwtAuth->signup($email,$pwd);
        }elseif($getToken != null){
            $signup = $jwtAuth->signup($email,$pwd, $getToken);
        }else{
            $signup = array(
                'status'=> 'error',
                'message' => 'Envia tus datos por post'
            );
        }

         return response()->json($signup, 200);

    }
}
