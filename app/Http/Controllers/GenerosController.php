<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genero;
use Illuminate\Support\Facades\Response;

class GenerosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $generos = Genero::all();
        $response = Response::json($generos, 200);
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if ((!$request->nombreGenero)||(!$request->imagenGenero)){
            $response = Response::json([
                'message' => 'Por favor escribe todos los datos'
            ], 422);
            return $response;
        }

        $genero = new Genero(array(
            'nombreGenero' => trim($request->nombreGenero),
            'imagenGenero' => trim($request->imagenGenero),

        ));

        $genero->save();
        $message = 'Genero creado con exito';

        $response = Response::json([
            'message' => $message,
            'data' => $genero
        ], 201);

        return $response;


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genero = Genero::find($id);

        if (!$genero){
            return Response::json([
                'error' => ['message' => "No existe el genero"]
            ], 404);
        }
        return Response::json($genero, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        print_r($request->nombreGenero);
        print_r($request->imagenGenero);

        if ((!$request->nombreGenero)||(!$request->imagenGenero)){
            $response = Response::json([
                'message' => 'Por favor escribe todos los datos'
            ], 422);
            return $response;
        }

        $genero = Genero::find($request->idGenero);

        if(!$genero){
            return Response::json([
                'error' => ['message' => 'El genero no existe']
            ], 404);
        }

        $genero->nombreGenero = trim($request->nombreGenero);
        $genero->imagenGenero = trim($request->imagenGenero);
        $genero->save();

        $message = 'El genero se ha modificado de forma correcta';

        $response = Response::json([
            'message' => $message,
            'data' => $genero
        ],201);

        return $response;
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idGenero){
        
        print_r($idGenero);

        $genero = Genero::find($idGenero);
        
        
        if(!$genero){
            return Response::json([
                'error' => ['message' => 'El genero no existe']
            ], 404);
        }else{
            $genero->delete();
            return response ()->json($genero);
        }
    }
}
