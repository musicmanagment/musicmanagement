<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Grupo;

class Genero extends Model{

    protected $fillable = [
        'nombreGenero', 'imagenGenero'
    ];

    protected $primaryKey = 'idGenero';

    public function grupos(){
        return $this->belongsToMany(Grupo::class);
    }

}
