<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multimedia extends Model
{
    public function grupos(){
        return $this->belongsTo('app\Grupo');
    }
}
