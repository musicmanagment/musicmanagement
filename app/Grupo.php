<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Genero;
use App\Usuario;

class Grupo extends Model{
    
    protected $primaryKey = 'idGrupo';

    protected $fillable = ['idGrupo', 'nombreGrupo', 'descripcion', 'imagenGrupoPerf', 'created_at','updated_at'];

    //protected $hidden = ['created_at','updated_at'];


    public function usuarios(){
        //return $this->hasMany(Usuario::class);
        return $this->belongsToMany(Usuario::class);
    }

    public function multimedia(){
        return $this->hasMany('app\Multimedia');
    }

    public function genero(){
        //return $this->hasMany(Genero::class);
        return $this->belongsToMany(Genero::class);
    }



}
