<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo_Multimedia extends Model
{
    protected $fillable = ['grupo_idGrupo','multimedia_idMultimedia', 'created_at','updated_at'];
}
