<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Grupo;

class Usuario extends Model{

    protected $fillable = [
        'nombreUsuario', 'apellidos','direccion', 'tipusUsuari', 'email', 'password', 'adminGrupo'
    ];
    
    /*public function grupos(){
        return $this->belongsTo('app\Grupo');
    }*/

    public function grupos(){
        return $this->belongsToMany(Grupo::class);
    }
}
