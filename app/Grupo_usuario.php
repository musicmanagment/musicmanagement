<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo_usuario extends Model
{
        protected $fillable = ['grupo_idGrupo','usuario_id', 'created_at','updated_at'];

}
