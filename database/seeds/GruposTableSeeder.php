<?php

use Illuminate\Database\Seeder;
use App\Grupo;
use Faker\Factory as Faker;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $faker = Faker::create();

        for($i = 0; $i<=4; $i++){
            Grupo::create([
                'idGrupo' => $faker->randomDigitNotNull,
                'nombreGRupo' => $faker->text(20),
            ]);
        }
    }
}
