<?php

use Illuminate\Database\Seeder;
use App\Genero;
use Faker\Factory as Faker;

class GenerosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){


        DB::table('generos')->insert([
            'nombreGenero' => str_random(20)
        ]);

        
    }
}
