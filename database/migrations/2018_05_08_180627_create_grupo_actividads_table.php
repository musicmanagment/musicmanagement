<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrupoActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_actividads', function (Blueprint $table) {
            $table->integer('idGrupo')->unsigned();
            //$table->primary('idGrupo');
            $table->date('fecha');
            $table->dateTime('horaInicio');
            //$table->primary('horaInicio');
            $table->dateTime('horaFin');
            $table->string('descripcionActividad');
            $table->integer('idTipoActividad')->unsigned();
            $table->string('direccion');
            $table->integer('idUsuario')->unsigned();
            $table->timestamps();
            $table->primary(['idGrupo', 'fecha','horaInicio']);
            
        });
        
        Schema::table('grupo_actividads', function($table) {
            $table->foreign('idGrupo')->references('idGrupo')->on('grupos')->onDelete('cascade');
            $table->foreign('idTipoActividad')->references('idTipoActividad')->on('tipo_actividads');
            $table->foreign('idUsuario')->references('idUsuario')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_actividads');
    }
}
