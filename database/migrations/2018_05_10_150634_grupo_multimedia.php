<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoMultimedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('grupo_multimedia', function(Blueprint $table){
            $table->integer('grupo_idGrupo')->unsigned()->index();
            $table->foreign('grupo_idGrupo')->references('idGrupo')->on('grupos')->onDelete('cascade');

            $table->integer('multimedia_idMultimedia')->unsigned()->index();
            $table->foreign('multimedia_idMultimedia')->references('idMultimedia')->on('multimedia')->onDelete('cascade');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('grupo_multimedia');
    }
}
