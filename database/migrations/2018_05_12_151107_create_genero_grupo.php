<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneroGrupo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('genero_grupo', function(Blueprint $table){
            
            $table->integer('genero_idGenero')->unsigned()->index();
            $table->foreign('genero_idGenero')->references('idGenero')->on('generos')->onDelete('cascade');
            
            $table->integer('grupo_idGrupo')->unsigned()->index();
            $table->foreign('grupo_idGrupo')->references('idGrupo')->on('grupos')->onDelete('cascade');


            $table->timestamps();
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('genero_grupo');
    }
}
