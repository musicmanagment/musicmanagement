<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_usuario', function(Blueprint $table){
            $table->integer('grupo_idGrupo')->unsigned()->index();
            $table->foreign('grupo_idGrupo')->references('idGrupo')->on('grupos')->onDelete('cascade');
            $table->integer('usuario_idUsuario')->unsigned()->index();
            $table->foreign('usuario_idUsuario')->references('idUsuario')->on('usuarios')->onDelete('cascade');
            $table->integer('test');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_usuario');
    }
}
